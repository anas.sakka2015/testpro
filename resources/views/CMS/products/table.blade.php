@foreach ($data as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td> <img class=" rounded" width="100" height="100"
                src="@if (substr($item->image, 0, 8) != 'https://') {{ asset('storage/' . $item->image) }} @else {{ asset($item->image) }} @endif"
                alt=""></td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->description }}</td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('products.edit', $item->id) }}"><i
                            class="bx bx-edit-alt me-1"></i>
                        Edit</a>
                    <a class="dropdown-item deleteItem" data-url='{{ route('products.destroy', $item->id) }}'
                        href="#"><i class="bx bx-trash me-1"></i>
                        Delete</a>
                </div>
            </div>
        </td>
    </tr>
@endforeach
