<div class="bs-toast toast fade show" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
        <i class="bx bx-bell me-2"></i>
        <div class="me-auto fw-semibold">{{ $notification->sender_id }}</div>
        <small>{{ $notification->created_at->diffForHumans() }}</small>
        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body">
        {{ $notification->message }}
    </div>
</div>
