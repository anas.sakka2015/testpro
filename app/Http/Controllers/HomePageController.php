<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('frontEnd.home.index', compact('products'));
    }
    public function show(Product $product)
    {
        return view('frontEnd.home.show', compact('product'));
    }
}
