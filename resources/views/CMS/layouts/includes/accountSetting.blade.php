<ul class="nav nav-pills flex-column flex-md-row mb-3">
    <li class="nav-item">
        <a class="nav-link {{ Route::is('accountSetting.index') ? 'active' : '' }}"
            href="{{ route('accountSetting.index') }}"><i class="bx bx-user me-1"></i> Account</a>
    </li>
        <li class="nav-item">
            <a class="nav-link {{ Route::is('notification.receiverMessage') ? 'active' : '' }}"
                href="{{ route('notification.receiverMessage') }}"><i class="bx bx-bell me-1"></i>
                Notifications</a>
        </li>
</ul>
