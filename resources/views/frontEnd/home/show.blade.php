@extends('frontEnd.layouts.app')
@section('title')
    show product
@endsection
@section('content')
    <div class="container p-0 mb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="row">
            <div class="col-8 offset-2 text-center">
                <h1 class=" mb-4">{{ $product->name }}</h1>
                <img class=" rounded mb-4"
                    src=" @if (substr($product->image, 0, 8) != 'https://') {{ asset('storage/' . $product->image) }} @else {{ asset($product->image) }} @endif "
                    alt="Card image cap" style="width: 300px ">
                <p class="card-text">{{ $product->description }}.</p>

            </div>

        </div>
    </div>
@endsection
