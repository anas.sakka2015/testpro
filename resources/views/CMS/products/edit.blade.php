@extends('CMS.layouts.app')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <form action="{{ route('products.update', $product->id) }}" method="post" class="p-4"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <h5 class="card-header">Update Product Information</h5>
                <div class="card-body demo-vertical-spacing demo-only-element">
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAcxJREFUSEvlle0xBEEQhp+LgAwQASJABmRABIgAESACIZABIkAEhEAE1KNmrub2ZmZ7+eGHrrrarduefvt9pz9m/JHN/giXnwLvApsp6WfgcSqBqcD7wA2wOgB6B46Au2gCU4CvgOORwKeAfqMWBVba+xRNWQ3+AMhUFU6AnfR9L33rgkeBvUfv9AXYakR8A9YAfbfHKEeBP1OgnpTnwFnyG4076gCUMvdkjPp95xYBXgdeE5ODTuUeporXdQNQ+qZFgD1sEa0A16mQagFz1X9U2m3JPwpctlJN7lLmXnLzBKLADgzbJ08r363e3E650ntVv8A6Cuwh79rJlMGH8tnf3nP3bvOhKcD5jMPCoZFZytyEQhPrN8BjsyH0PcpYdkrsszW5ZO7Pe/b5q5FpUbmNlHaKKb3byuKrWo9xbQXaoy02KmGvZ+uuyhawQZ6KIPamxTNWsVa+xVeuTxfGUrIt4LyNZChz+3aKeeY2HahuqxpwuWXCi72Slcwv0/8XgHHnVgOWnUs9PIU6UmTlHC6O1S5w3r1LWU7ROvk2d/SQcVlUvRUYzaG864UiGwJPWuYB9Ga8IbADo5zBzQEQANWlGS86MoM4cbf/B/wF5GpcH3yxaxIAAAAASUVORK5CYII=" />
                        {{-- <span >Full name</span> --}}
                        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                            placeholder="Name" aria-label="Name" value="{{ $product->name }}" name='name' required />
                        @if ($errors->has('name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAcxJREFUSEvlle0xBEEQhp+LgAwQASJABmRABIgAESACIZABIkAEhEAE1KNmrub2ZmZ7+eGHrrrarduefvt9pz9m/JHN/giXnwLvApsp6WfgcSqBqcD7wA2wOgB6B46Au2gCU4CvgOORwKeAfqMWBVba+xRNWQ3+AMhUFU6AnfR9L33rgkeBvUfv9AXYakR8A9YAfbfHKEeBP1OgnpTnwFnyG4076gCUMvdkjPp95xYBXgdeE5ODTuUeporXdQNQ+qZFgD1sEa0A16mQagFz1X9U2m3JPwpctlJN7lLmXnLzBKLADgzbJ08r363e3E650ntVv8A6Cuwh79rJlMGH8tnf3nP3bvOhKcD5jMPCoZFZytyEQhPrN8BjsyH0PcpYdkrsszW5ZO7Pe/b5q5FpUbmNlHaKKb3byuKrWo9xbQXaoy02KmGvZ+uuyhawQZ6KIPamxTNWsVa+xVeuTxfGUrIt4LyNZChz+3aKeeY2HahuqxpwuWXCi72Slcwv0/8XgHHnVgOWnUs9PIU6UmTlHC6O1S5w3r1LWU7ROvk2d/SQcVlUvRUYzaG864UiGwJPWuYB9Ga8IbADo5zBzQEQANWlGS86MoM4cbf/B/wF5GpcH3yxaxIAAAAASUVORK5CYII=" />
                        {{-- <span >Full name</span> --}}
                        <textarea type="text" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}"
                            placeholder="Description" aria-label="Description" value="" name='description' required>{{ $product->description }}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <img src="@if (substr($product->image, 0, 8) != 'https://') {{ asset('storage/' . $product->image) }} @else {{ $product->image }} @endif"
                            id="output" class="rounded my-2" alt="" width="150px" height="150px">
                        <label class="form-label w-100 " for="basic-default-password12">Image</label><br>
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAPVJREFUSEvtl40RwUAQhb90oAM6QAXogA6UoAQ6oQI6QAdKoAJKMC9zMXETdzkuMsztTCaTZPPe7mb/ktGSZC3x4iKeAv0Iht2ADaDzQ6qIB8AW6EUgLSBOwNBHLKUYntp2i1jYudged4Drm54egbH1rq735t4EOLwiLiuG8idiRSyF+j+TS41nZLrVV76xmoOOuanDM7ADFk3Xsa/uG2sgidiOQAr1U0Q+mU6/n1wXYO1zI+C5sNRUKjeQcqhnpvMEYNdXda0+S2BVHypMs2rLbGrZc9axzNZ00dzshvng1fYSFwha6LVjxxJncsUi8eK09u90Bw1yTB+z/MtBAAAAAElFTkSuQmCC" />
                        <input type="file"
                            onchange="document.getElementById('output').src = window.URL.createObjectURL(this.files[0])"
                            class="form-control" placeholder="Image" aria-label="image" name="image" value="" />
                    </div>

                    <button class="btn btn-primary btn-style" type="submit">Update</button>

                </div>
            </form>
        </div>
    </div>
@endsection
