<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignUpRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        $credentials = $request->validated();

        if (!Auth::attempt($credentials)) {
            return redirect()->back()->withInput($request->validated())->with('error', 'Your password is incorrect');
        }
        return to_route('products.index')->with('success', 'Log in successfully');
    }
    public function signUpIndex()
    {
        return view('auth.signUpIndex');
    }
    public function signUp(SignUpRequest $request)
    {
        $credentials = $request->validated();
        $data = $request->validated();
        $data['password'] = Hash::make('password');
        $user = User::create($data);
        auth()->login($user);
        return to_route('products.index')->with('success', 'Log in successfully');
    }
    public function logout()
    {

        Auth::logout();

        return redirect('login');
    }
}
