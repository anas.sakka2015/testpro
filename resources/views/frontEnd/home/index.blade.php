  @extends('frontEnd.layouts.app')
  @section('title')
      HomePage
  @endsection
  @section('content')
      <div class="container  mb-5 wow fadeIn" data-wow-delay="0.1s">
          <div class="row text-center">
              @foreach ($products as $product)
                  <div class="col-3  my-4">
                      <div class="card text-center h-100 " style="width: 18rem;">
                          <img class="card-img-top"
                              src=" @if (substr($product->image, 0, 8) != 'https://') {{ asset('storage/' . $product->image) }} @else {{ asset($product->image) }} @endif "
                              alt="Card image cap">
                          <div class="card-body">
                              <h5 class="card-title">{{ $product->name }}</h5>
                              <p class="card-text">{{ $product->description }}.</p>
                              <a href="{{ route('product.show', $product->id) }}" class="btn btn-primary">show</a>
                          </div>
                      </div>
                  </div>
              @endforeach

          </div>
      </div>
  @endsection
